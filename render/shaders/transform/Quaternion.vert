#version 460 core

struct Quaternion {
    float   w;
    vec3    v;
};

Quaternion quaternion_scale(in Quaternion q, float rate) {
    return Quaternion(rate * q.w, q.v * rate);
}

Quaternion quatrnion_unitize(in Quaternion q) {
    vec4 temp = vec4(q.w, q.v);
    float rate = sqrt(dot(temp, temp));
    return quaternion_scale(q, rate);
}

Quaternion quaternion_conjugate(in Quaternion q) {
    return Quaternion(q.w, - q.v);
}

Quaternion quaternion_add(in Quaternion q, in Quaternion p) {
    return Quaternion(q.w + p.w, q.v + p.v);
}

Quaternion quaternion_subtract(in Quaternion q, in Quaternion p) {
    return Quaternion(q.w - p.w, q.v - p.v);
}

Quaternion quaternion_multiply(in Quaternion q, in Quaternion p) {
    return Quaternion(
        q.w * p.w - dot(q.v, p.v),
        q.w * p.v + p.w * q.v + cross(q.v, p.v)
    );
}

vec3 quaternion_rotate(Quaternion q, vec3 v) {
    Quaternion p = Quaternion(0, v);
    p = quaternion_multiply(q, p);
    p = quaternion_multiply(p, quaternion_conjugate(q));
    return p.v;
}

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 a_tex_coord;
uniform Quaternion rotate_quaternion;

out vec2 tex_coord;

void main() {
    vec3 rotated_pos = quaternion_rotate(rotate_quaternion, position);
    gl_Position = vec4(rotated_pos, 1.0);
    tex_coord = vec2(a_tex_coord.x, a_tex_coord.y);
}
