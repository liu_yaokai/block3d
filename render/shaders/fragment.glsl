#version 460 core

out vec4 FragColor;
in vec2 tex_coord;

uniform sampler2D my_texture;

void main() {
    FragColor = texture2D(my_texture, tex_coord);
//    FragColor = vec4(1.0f, 0.4f, 0.2f, 1.0f);
}
