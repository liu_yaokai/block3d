//
// Created by Yaokai Liu on 1/30/23.
//

#include "Block/Block.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>
#include "debug.h"
#include "Quaternion.h"
#include "DualQuaternion.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#define len_of(_array) (sizeof(_array) / sizeof(_array[0]))

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

typedef struct {
    Vector3 position;
    Vector2 tex_coord;
} vertex;

#define NO_OFFSET (0)

int main(int argc, char *argv[]) {
    int success;
    char infoLog[512];
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    GLFWwindow *window = glfwCreateWindow(800, 800, "Block3D", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        fprintf(stderr, "Failed to initialize GLAD\n");
        return -1;
    }
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(message_callback, NULL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLuint program = glCreateProgram();
    {
        char *pvs = calloc(4096, sizeof(char));
        char *pfs = calloc(4096, sizeof(char));
        FILE *file;

        file = fopen("shaders/transform/Quaternion.vert", "r");
        fread(pvs, sizeof(GLchar), 4096, file);
        fclose(file);
        GLuint VS = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(VS, 1, (const GLchar *const *) &pvs, NULL);
        glCompileShader(VS);
        glGetShaderiv(VS, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(VS, 512, NULL, infoLog);
            fprintf(stderr, "%s\n", infoLog);
            glfwTerminate();
            return -1;
        }
        glAttachShader(program, VS);

        file = fopen("shaders/fragment.glsl", "r");
        fread(pfs, sizeof(GLchar), 512, file);
        fclose(file);
        GLuint FS = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(FS, 1, (const GLchar *const *) &pfs, NULL);
        glCompileShader(FS);
        glGetShaderiv(FS, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(FS, 512, NULL, infoLog);
            fprintf(stderr, "%s\n", infoLog);
            glfwTerminate();
            return -1;
        }
        glAttachShader(program, FS);

        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(program, 512, NULL, infoLog);
            fprintf(stderr, "%s\n", infoLog);
            glfwTerminate();
            return -1;
        }
        glDeleteShader(VS);
        glDeleteShader(FS);
        free(pvs);
        free(pfs);
        glBindFragDataLocation(program, 0, "outColor");
    }
    glUseProgram(program);

    vertex vertices[] = {
            {{-0.5f, -0.5f, 0.5f},  {0.0f, 0.0f}},
            {{-0.5f, 0.5f,  0.5f},  {1.0f, 0.0f}},
            {{0.5f,  0.5f,  0.5f},  {1.0f, 1.0f}},
            {{0.5f,  -0.5f, 0.5f},  {0.0f, 1.0f}},

            {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}},
            {{-0.5f, 0.5f,  -0.5f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f,  -0.5f}, {1.0f, 1.0f}},
            {{0.5f,  -0.5f, -0.5f}, {0.0f, 1.0f}},
    };
    GLuint indices[] = {
            0, 1, 2, 2, 3, 0,
            4, 5, 6, 6, 7, 4,
//            0, 4, 5, 5, 1, 0,
//            1, 5, 6, 6, 2, 1,
//            2, 6, 7, 7, 3, 2,
//            3, 7, 4, 4, 0, 3,
    };
//    GLuint indices[] = {
//            0, 1, 1, 2, 2, 3, 3, 0,
//            4, 5, 5, 6, 6, 7, 7, 4,
//            0, 4, 1, 5, 2, 6, 3, 7
//    };
    GLuint VBOs[1] = {}, EBOs[1] = {};
    glCreateBuffers(1, VBOs);
    glCreateBuffers(1, EBOs);
    glNamedBufferData(VBOs[0], sizeof(vertices), vertices, GL_STATIC_DRAW);
    glNamedBufferData(EBOs[0], sizeof(indices), indices, GL_STATIC_DRAW);

    GLuint VAOs[1] = {};
    glCreateVertexArrays(1, VAOs);
    GLint position_bind_point = 0;
    GLint position_layout = glGetAttribLocation(program, "position");
    glVertexArrayAttribFormat(VAOs[0], position_layout, 3, GL_FLOAT, GL_FALSE, offsetof(vertex, position));
    glVertexArrayAttribBinding(VAOs[0], position_layout, position_bind_point);
    glVertexArrayVertexBuffer(VAOs[0], position_bind_point, VBOs[0], NO_OFFSET, sizeof(vertex));
    glEnableVertexArrayAttrib(VAOs[0], position_layout);
    GLint tex_coord_bind_point = 1;
    GLint tex_coord_layout = glGetAttribLocation(program, "a_tex_coord");
    glVertexArrayAttribFormat(VAOs[0], tex_coord_layout, 2, GL_FLOAT, GL_FALSE, offsetof(vertex, tex_coord));
    glVertexArrayAttribBinding(VAOs[0], tex_coord_layout, tex_coord_bind_point);
    glVertexArrayVertexBuffer(VAOs[0], tex_coord_bind_point, VBOs[0], NO_OFFSET, sizeof(vertex));
    glEnableVertexArrayAttrib(VAOs[0], tex_coord_layout);

    glVertexArrayElementBuffer(VAOs[0], EBOs[0]);

    glActiveTexture(GL_TEXTURE0);
    GLint texture_id = glGetUniformLocation(program, "my_texture");
    glUniform1i(texture_id, 0);
    GLuint TEXs[1] = {};
    glCreateTextures(GL_TEXTURE_2D, 1, TEXs);
    glBindTexture(GL_TEXTURE_2D, TEXs[0]);
//    glActiveTexture(GL_TEXTURE0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    int texture_width, texture_height, texture_n_channels;
    unsigned char *texture_graph = stbi_load("textures/example.jpg", &texture_width, &texture_height,
                                             &texture_n_channels, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture_width, texture_height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_graph);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(texture_graph);

    glBindTexture(GL_TEXTURE_2D, TEXs[0]);
    glBindVertexArray(VAOs[0]);


    GLint rotate_w = glGetUniformLocation(program, "rotate_quaternion.w");
    GLint rotate_v = glGetUniformLocation(program, "rotate_quaternion.v");

    Quaternion quaternion = {}, temp;
    quaternionFromRotate((Vector3) {1.0f, 0.0f, 0.0f}, 0.0f, &quaternion, true);
    glUniform1f(rotate_w, quaternion.w);
    glUniform3f(rotate_v, quaternion.v[0], quaternion.v[1], quaternion.v[2]);
//    glLineWidth(4);
    GLdouble start = glfwGetTime();
    while (!glfwWindowShouldClose(window)) {
        processInput(window);
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        quaternionFromRotate((Vector3) {1.0f, 1.0f, 1.0f},
                             fmod(glfwGetTime() - start, 2 * M_PI) + M_PI / 4,
                             &quaternion, true);
        glUniform1f(rotate_w, (GLfloat) quaternion.w);
        glUniform3fv(rotate_v, 1, quaternion.v);
        glUseProgram(program);
        glBindTexture(GL_TEXTURE_2D, TEXs[0]);
        glBindVertexArray(VAOs[0]);
        glDrawElements(GL_TRIANGLES, len_of(indices), GL_UNSIGNED_INT, 0);
//        glDrawElements(GL_LINES, len_of(indices), GL_UNSIGNED_INT, 0);
        glfwSwapBuffers(window);
        glfwPollEvents();
        int error = glGetError();
        if (error) {
            glfwTerminate();
            return error;
        }
    }
    glfwTerminate();
    return 0;
}
