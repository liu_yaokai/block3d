//
// Created by Yaokai Liu on 2023/2/21.
//

#ifndef BLOCK3D_WINDOW_H
#define BLOCK3D_WINDOW_H

#include "GLFW/glfw3.h"

void frame_buffer_size_callback(GLFWwindow *window, int width, int height);

#endif //BLOCK3D_WINDOW_H
