//
// Created by Yaokai Liu on 2023/2/22.
//

#include "render.h"
#include "window.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "local.h"


GLFWwindow *game_window_init() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    GLFWwindow *window = glfwCreateWindow(800, 600, "Block3D", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        return NULL;
    }
    glfwMakeContextCurrent(window);
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        fprintf(stderr, "Failed to initialize GLAD\n");
        return NULL;
    }
    glfwSetFramebufferSizeCallback(window, frame_buffer_size_callback);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(message_callback, NULL);

    return window;
}

GLuint game_shader_init(const char *shader_dir) {
    int status;
    char infoLog[512];
    char shader_path[512] = {};

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLuint program = glCreateProgram();
    char *pvs = calloc(4096, sizeof(char));
    char *pfs = calloc(4096, sizeof(char));
    FILE *file;

    strcpy(shader_path, shader_dir);
    strcpy(shader_path + strlen(shader_dir), "/transform/Quaternion.vert");
    file = fopen(shader_path, "r");
    fread(pvs, sizeof(GLchar), 4096, file);
    fclose(file);
    GLuint VS = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(VS, 1, (const GLchar *const *) &pvs, NULL);
    glCompileShader(VS);
    glGetShaderiv(VS, GL_COMPILE_STATUS, &status);
    if (!status) {
        glGetShaderInfoLog(VS, 512, NULL, infoLog);
        fprintf(stderr, "%s\n", infoLog);
        glfwTerminate();
        return -1;
    }
    glAttachShader(program, VS);

    strcpy(shader_path, shader_dir);
    strcpy(shader_path + strlen(shader_dir), "/fragment.glsl");
    file = fopen(shader_path, "r");
    fread(pfs, sizeof(GLchar), 512, file);
    fclose(file);
    GLuint FS = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(FS, 1, (const GLchar *const *) &pfs, NULL);
    glCompileShader(FS);
    glGetShaderiv(FS, GL_COMPILE_STATUS, &status);
    if (!status) {
        glGetShaderInfoLog(FS, 512, NULL, infoLog);
        fprintf(stderr, "%s\n", infoLog);
        glfwTerminate();
        return -1;
    }
    glAttachShader(program, FS);

    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (!status) {
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        fprintf(stderr, "%s\n", infoLog);
        glfwTerminate();
        return -1;
    }
    glDeleteShader(VS);
    glDeleteShader(FS);
    free(pvs);
    free(pfs);
    glBindFragDataLocation(program, 0, "outColor");
    return program;
}

int set_camera() {
    return 0;
}


int compute_objects() {
    return 0;
}


int game_render_update() {
    return 0;
}


int game_terminate() {
    glfwTerminate();
    return 0;
}