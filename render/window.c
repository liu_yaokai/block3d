//
// Created by Yaokai Liu on 2023/2/21.
//

#include "window.h"

void frame_buffer_size_callback(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}
