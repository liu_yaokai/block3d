//
// Created by Yaokai Liu on 2/1/23.
//

#ifndef BLOCK3D_DEBUG_H
#define BLOCK3D_DEBUG_H

#include <glad/glad.h>
void message_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const *message,
                      void const *userParam);

#endif //BLOCK3D_DEBUG_H
