//
// Created by Yaokai Liu on 2/2/23.
//

#ifndef BLOCK3D_VECTOR_H
#define BLOCK3D_VECTOR_H

#include <stdbool.h>
#include <float.h>
#include <math.h>

typedef float Vector2[2];
typedef float Vector3[3];
typedef float Vector4[4];

void vector2Unitize(Vector2 vector);
float vector2GetNorm(Vector2 vector);
float vector2DotMultiply(const Vector2 vector1, const Vector2 vector2);
void vector2Scale(Vector2 vector, float rate);

float vector3GetNorm(Vector3 vector);
void vector3Unitize(Vector3 vector);
void vector3Copy(const Vector3 target, Vector3 dest);
void vector3Scale(Vector3 vector, float rate);
void vector3ScaleCopy(const Vector3 target, float rate, Vector3 dest);
void vector3Add(const Vector3 addend, Vector3 summand);
float vector3DotMultiply(const Vector3 multiplier, const Vector3 multiplicand);
void vector3CrossMultiply(const Vector3 multiplier, Vector3 multiplicand);

float vector4GetNorm(Vector4 vector);
float vector4DotMultiply(const Vector4 multiplier, const Vector4 multiplicand);

#endif //BLOCK3D_VECTOR_H
