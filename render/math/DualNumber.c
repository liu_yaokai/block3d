//
// Created by Yaokai Liu on 2/4/23.
//

#include "DualNumber.h"

void inline dualNumberDual(DualNumber *dual_number) {
    dual_number->e = - dual_number->e;
}

void inline dualNumberInverse(DualNumber *dual_number) {
    dual_number->o = 1 / (dual_number->o);
    dual_number->e = - dual_number->o * dual_number->e / dual_number->o;
}

void inline dualNumberAdd(DualNumber addend, DualNumber *summand) {
    summand->o += addend.o;
    summand->e += addend.e;
}

void inline dualNumberSubtract(DualNumber subtrahend, DualNumber *minuend) {
    minuend->o -= subtrahend.o;
    minuend->e -= subtrahend.e;
}

void inline dualNumberMultiply(DualNumber multiplier, DualNumber *multiplicand) {
    float temp = multiplicand->o;
    multiplicand->o *= multiplier.o;
    multiplicand->e *= multiplier.o;
    multiplicand->e += temp * multiplier.e;
}

void inline dualNumberDivision(DualNumber divisor, DualNumber *dividend) {
    dividend->o /= divisor.o;
    dividend->e /= divisor.o;
    dividend->e -= dividend->o * divisor.e / divisor.o;
}
