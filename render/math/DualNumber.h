//
// Created by Yaokai Liu on 2/4/23.
//

#ifndef BLOCK3D_DUALNUMBER_H
#define BLOCK3D_DUALNUMBER_H

typedef struct {
    float      o;
    float      e;
} DualNumber;

void dualNumberDual(DualNumber *dual_number);
void dualNumberInverse(DualNumber *dual_number);
void dualNumberAdd(DualNumber addend, DualNumber *summand);
void dualNumberSubtract(DualNumber subtrahend, DualNumber *minuend);
void dualNumberMultiply(DualNumber multiplier, DualNumber *multiplicand);
void dualNumberDivision(DualNumber divisor, DualNumber *dividend);


#endif //BLOCK3D_DUALNUMBER_H
