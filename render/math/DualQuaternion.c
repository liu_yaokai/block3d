//
// Created by Yaokai Liu on 2/4/23.
//

#include "DualQuaternion.h"

#define len_of(_array) (sizeof(_array) / sizeof((_array)[0]))

bool inline dualQuaternionEqual(DualQuaternion a, DualQuaternion b) {
	return quaternionEqual(a.o, b.o) && quaternionEqual(a.e, b.e);
}

void inline dualQuaternionCopy(DualQuaternion target, DualQuaternion *dest) {
    quaternionCopy(target.o, &(dest->o));
    quaternionCopy(target.e, &(dest->e));
}

void inline dualQuaternionScaleCopy(DualQuaternion target, DualQuaternion *dest, float rate) {
    quaternionScaleCopy(target.o, &(dest->o), rate);
    quaternionScaleCopy(target.e, &(dest->e), rate);
}

void inline dualQuaternionScale(DualQuaternion *dual_quaternion, float rate) {
    quaternionScale(&(dual_quaternion->o), rate);
    quaternionScale(&(dual_quaternion->e), rate);
}

void inline dualQuaternionNegative(DualQuaternion *dual_quaternion) {
    quaternionNegative(&(dual_quaternion->o));
    quaternionNegative(&(dual_quaternion->e));
}

void inline dualQuaternionGetNorm(DualQuaternion dualQuaternion, DualNumber * dest) {
    dest->o = vector4DotMultiply(*(Vector4 *)&(dualQuaternion.o), *(Vector4 *)&(dualQuaternion.o));
    dest->e = vector4DotMultiply(*(Vector4 *)&(dualQuaternion.o), *(Vector4 *)&(dualQuaternion.e));
    dest->o = sqrtf(dest->o);
    dest->e /= dest->o;
}

void inline dualQuaternionUnitize(DualQuaternion *dual_quaternion) {
    DualNumber norm = {};
    dualQuaternionGetNorm(*dual_quaternion, &norm);
    quaternionScale(&(dual_quaternion->o), norm.e);
    quaternionSubtract(dual_quaternion->o, &(dual_quaternion->e));
    quaternionUnitize(&(dual_quaternion->o));
}

void inline dualQuaternionConjugate(DualQuaternion *dual_quaternion) {
    quaternionConjugate(&(dual_quaternion->o));
    quaternionConjugate(&(dual_quaternion->e));
}

void inline dualQuaternionDual(DualQuaternion *dual_quaternion) {
    quaternionNegative(&(dual_quaternion->e));
}


void inline dualQuaternionInverse(DualQuaternion *dual_quaternion) {
    DualNumber dest = {};
    dest.o = vector4DotMultiply(*(Vector4 *)&(dual_quaternion->o), *(Vector4 *)&(dual_quaternion->o));
    dest.e = 2 * vector4DotMultiply(*(Vector4 *)&(dual_quaternion->o), *(Vector4 *)&(dual_quaternion->e));

}

void inline dualQuaternionAdd(DualQuaternion addend, DualQuaternion *summand) {
    quaternionAdd(addend.o, &(summand->o));
    quaternionAdd(addend.e, &(summand->e));
}

void inline dualQuaternionSubtract(DualQuaternion subtrahend, DualQuaternion *minuend) {
    quaternionSubtract(subtrahend.o, &(minuend->o));
    quaternionSubtract(subtrahend.e, &(minuend->e));
}

void inline dualQuaternionMultiply(DualQuaternion multiplier, DualQuaternion *multiplicand) {
    Quaternion temp = {};
    quaternionCopy(multiplicand->o, &temp);
    quaternionMultiply(multiplier.o, &(multiplicand->o));
    quaternionMultiply(multiplier.o, &(multiplicand->e));
    quaternionMultiply(multiplier.e, &temp);
    quaternionAdd(temp, &(multiplicand->e));
}

void inline dualQuaternionFromTransform(Vector3 axis, float angle, Vector3 displacement,
                                        DualQuaternion * dual_quaternion) {
    quaternionFromRotate(axis, angle, &(dual_quaternion->o), 0);
    vector3Copy(displacement, dual_quaternion->e.v);
    dual_quaternion->e.w = 0;
    quaternionMultiply(dual_quaternion->o, &(dual_quaternion->e));
    quaternionScale(&(dual_quaternion->e), 1.0f / 2);
}

void dualQuaternionTransform(DualQuaternion dual_quaternion, Vector3 vector) {
    DualQuaternion dual_conj = {};
    DualQuaternion ext_vec = {.o.w = 1};
    vector3Copy(vector, ext_vec.e.v);
    dualQuaternionCopy(dual_quaternion, &dual_conj);
    dualQuaternionConjugate(&dual_conj);
    dualQuaternionDual(&dual_conj);
    dualQuaternionMultiply(ext_vec, &dual_quaternion);
    dualQuaternionMultiply(dual_conj, &dual_quaternion);
    vector3Copy(dual_quaternion.e.v, vector);
}
