//
// Created by Yaokai Liu on 2/2/23.
//

#ifndef BLOCK3D_QUATERNION_H
#define BLOCK3D_QUATERNION_H

#include <stdbool.h>
#include <float.h>
#include <math.h>

#include "Vector.h"

typedef struct {
    float w;
    Vector3 v;
} Quaternion;

bool quaternionEqual(Quaternion a, Quaternion b);
void quaternionCopy(Quaternion target, Quaternion *dest);
void quaternionScaleCopy(Quaternion target, Quaternion *dest, float rate);
void quaternionScale(Quaternion *quaternion, float rate);
void quaternionNegative(Quaternion *quaternion);
void quaternionUnitize(Quaternion *quaternion);
void quaternionConjugate(Quaternion *quaternion);
void quaternionInverse(Quaternion *quaternion);
void quaternionAdd(Quaternion addend, Quaternion *summand);
void quaternionSubtract(Quaternion subtrahend, Quaternion *minuend);
void quaternionMultiply(Quaternion multiplier, Quaternion *multiplicand);
void quaternionDivision(Quaternion divisor, Quaternion *dividend);
void quaternionFromRotate(Vector3 axis, float angle, Quaternion *quaternion, bool unitize);
void quaternionMultiRotate(Vector3 axis, float angle, Quaternion *quaternion, bool unitize);
void quaternionRotate(Quaternion quaternion, Vector3 vector);


#endif //BLOCK3D_QUATERNION_H
