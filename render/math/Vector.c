//
// Created by Yaokai Liu on 2/2/23.
//

#include "Vector.h"

#define len_of(_array) (sizeof(_array) / sizeof((_array)[0]))

float vector3GetNorm(Vector3 vector) {
    return sqrtf(vector3DotMultiply(vector, vector));
}
float vector4GetNorm(Vector4 vector) {
    return sqrtf(vector4DotMultiply(vector, vector));
}

void vector2Unitize(Vector2 vector) {
    float norm = vector2GetNorm(vector);
    if (norm != 1.0f) {
        vector2Scale(vector, 1.0f / norm);
    }
}

void vector2Scale(Vector2 vector, float rate) {
    for (int i = 0; i < len_of((Vector2) {}); i++) {
        vector[i] *= rate;
    }
}

float vector2GetNorm(Vector2 vector) {
    return sqrtf(vector2DotMultiply(vector, vector));
}

float vector2DotMultiply(const Vector2 multiplier, const Vector2 multiplicand) {
    float temp = 0;
    for (int i = 0; i < len_of((Vector2) {});
    i++) {
        temp += multiplier[i] * multiplicand[i];
    }
    return temp;
}

void vector3Unitize(Vector3 vector) {
    float norm = vector3GetNorm(vector);
    if (norm != 1.0f) {
        vector3Scale(vector, 1.0f / norm);
    }
}

void inline vector3Copy(const Vector3 target, Vector3 dest) {
    for (int i = 0; i < len_of((Vector3) {});
    i++) {
        dest[i] = target[i];
    }
}

void vector3Scale(Vector3 vector, float rate) {
    for (int i = 0; i < len_of((Vector3) {});
    i++) {
        vector[i] *= rate;
    }
}

void vector3ScaleCopy(const Vector3 target, float rate, Vector3 dest) {
    for (int i = 0; i < len_of((Vector3) {});
    i++) {
        dest[i] = rate * target[i];
    }
}

void inline vector3CrossMultiply(const Vector3 multiplier, Vector3 multiplicand) {
    Vector3 temp = {};
    for (int i = 0; i < len_of(temp); i++) {
        temp[i] = multiplicand[(i + 1) % len_of(temp)] * multiplier[(i + 2) % len_of(temp)]
                - multiplier[(i + 1) % len_of(temp)] * multiplicand[(i + 2) % len_of(temp)];
    }
    vector3Copy(temp, multiplicand);
}

float inline vector3DotMultiply(const Vector3 multiplier, const Vector3 multiplicand) {
    float temp = 0;
    for (int i = 0; i < len_of((Vector3) {});
    i++) {
        temp += multiplier[i] * multiplicand[i];
    }
    return temp;
}

void inline vector3Add(const Vector3 addend, Vector3 summand) {
    for (int i = 0; i < len_of((Vector3) {});
    i++) {
        summand[i] += addend[i];
    }
}

float inline vector4DotMultiply(const Vector4 multiplier, const Vector4 multiplicand) {
    float temp = 0;
    for (int i = 0; i < len_of((Vector4) {});
    i++) {
        temp += multiplier[i] * multiplicand[i];
    }
    return temp;
}
