//
// Created by Yaokai Liu on 2/4/23.
//

#ifndef BLOCK3D_DUALQUATERNION_H
#define BLOCK3D_DUALQUATERNION_H

#include "Quaternion.h"
#include "DualNumber.h"

typedef struct {
    Quaternion o;
    Quaternion e;
} DualQuaternion;

bool dualQuaternionEqual(DualQuaternion a, DualQuaternion b);
void dualQuaternionGetNorm(DualQuaternion dualQuaternion, DualNumber * dest);
void dualQuaternionCopy(DualQuaternion target, DualQuaternion *dest);
void dualQuaternionScaleCopy(DualQuaternion target, DualQuaternion *dest, float rate);
void dualQuaternionScale(DualQuaternion *dual_quaternion, float rate);
void dualQuaternionNegative(DualQuaternion *dual_quaternion);
void dualQuaternionUnitize(DualQuaternion *dual_quaternion);
void dualQuaternionConjugate(DualQuaternion *dual_quaternion);
void dualQuaternionDual(DualQuaternion *dual_quaternion);
void dualQuaternionInverse(DualQuaternion *dual_quaternion);
void dualQuaternionAdd(DualQuaternion addend, DualQuaternion *summand);
void dualQuaternionSubtract(DualQuaternion subtrahend, DualQuaternion *minuend);
void dualQuaternionMultiply(DualQuaternion multiplier, DualQuaternion *multiplicand);
void dualQuaternionFromTransform(Vector3 axis, float angle, Vector3 displacement,
                                 DualQuaternion * dual_quaternion);
void dualQuaternionTransform(DualQuaternion dual_quaternion, Vector3 vector);


#endif //BLOCK3D_DUALQUATERNION_H
