//
// Created by Yaokai Liu on 2/2/23.
//

#include "Quaternion.h"

#define len_of(_array) (sizeof(_array) / sizeof((_array)[0]))

bool inline quaternionEqual(const Quaternion a, const Quaternion b) {
    bool e = (a.w == b.w);
    for (int i = 0; i < len_of(a.v); i++) {
        e &= (a.v[i] == b.v[i]);
    }
    return e;
}

void inline quaternionCopy(const Quaternion target, Quaternion *dest) {
    dest->w = target.w;
    for (int i = 0; i < len_of(target.v); i++) {
        dest->v[i] = target.v[i];
    }
}

void inline quaternionScaleCopy(Quaternion target, Quaternion *dest, float rate) {
    dest->w = rate * target.w;
    for (int i = 0; i < len_of(target.v); i++) {
        dest->v[i] = rate * target.v[i];
    }
}

void inline quaternionScale(Quaternion *quaternion, float rate) {
    quaternion->w *= rate;
    for (int i = 0; i < len_of(quaternion->v); i++) {
        quaternion->v[i] *= rate;
    }
}

void inline quaternionNegative(Quaternion *quaternion) {
    quaternion->w = -quaternion->w;
    for (int i = 0; i < len_of(quaternion->v); i++) {
        quaternion->v[i] = -quaternion->v[i];
    }
}

void inline quaternionUnitize(Quaternion *quaternion) {
    float rate = vector4DotMultiply(
            *(Vector4 *) quaternion,
            *(Vector4 *) quaternion);
    rate = sqrtf(rate);
    quaternionScale(quaternion, 1.0f / rate);
}

void inline quaternionConjugate(Quaternion *quaternion) {
    for (int i = 0; i < len_of(quaternion->v); i++) {
        quaternion->v[i] = -quaternion->v[i];
    }
}

void inline quaternionInverse(Quaternion *quaternion) {
    float norm_square = vector4DotMultiply(*(Vector4 *) quaternion, *(Vector4 *) quaternion);
    quaternionConjugate(quaternion);
    quaternionScale(quaternion, 1 / norm_square);
}

void inline quaternionAdd(const Quaternion addend, Quaternion *summand) {
    summand->w += addend.w;
    for (int i = 0; i < len_of(addend.v); i++) {
        summand->v[i] += addend.v[i];
    }
}

void inline quaternionSubtract(const Quaternion subtrahend, Quaternion *minuend) {
    minuend->w -= subtrahend.w;
    for (int i = 0; i < len_of(subtrahend.v); i++) {
        minuend->v[i] -= subtrahend.v[i];
    }
}

void inline quaternionMultiply(const Quaternion multiplier, Quaternion *multiplicand) {
    Vector3 temp1 = {}, temp2  ={};
    vector3ScaleCopy(multiplier.v, multiplicand->w, temp1);
    vector3ScaleCopy(multiplicand->v, multiplier.w, temp2);
    multiplicand->w = multiplicand->w * multiplier.w - vector3DotMultiply(multiplier.v, multiplicand->v);
    vector3CrossMultiply(multiplier.v, multiplicand->v);
    vector3Add(temp1, multiplicand->v);
    vector3Add(temp2, multiplicand->v);
}

void inline quaternionDivision(Quaternion divisor, Quaternion *dividend) {
    quaternionInverse(&divisor);
    quaternionMultiply(*dividend, &divisor);
    quaternionCopy(divisor, dividend);
}

void inline quaternionFromRotate(Vector3 axis, float angle, Quaternion *quaternion, bool unitize) {
    float norm = (unitize) ? vector3GetNorm(axis) : 1;
    vector3ScaleCopy(axis,
                     sinf(angle / 2) / norm,
                     quaternion->v);
    quaternion->w = cosf(angle / 2);
}
void inline quaternionMultiRotate(Vector3 axis, float angle, Quaternion *quaternion, bool unitize) {
    Quaternion temp = {};
    quaternionFromRotate(axis, angle, &temp, unitize);
    quaternionMultiply(temp, quaternion);
}


void quaternionRotate(Quaternion quaternion, Vector3 vector) {
    Quaternion conjugate = {};
    Quaternion ext_vec = {.w = 0};
    vector3Copy(vector, ext_vec.v);
    quaternionCopy(quaternion, &conjugate);
    quaternionConjugate(&conjugate);
    quaternionMultiply(ext_vec, &quaternion);
    quaternionMultiply(conjugate, &quaternion);
    vector3Copy(quaternion.v, vector);
}
