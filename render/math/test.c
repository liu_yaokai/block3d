//
// Created by Yaokai Liu on 2/2/23.
//

#include "Vector.h"
#include "Quaternion.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
    Quaternion a = {0, {-2, 0, 0}};
    Quaternion b = {0, {0, 2, 0}};
    printf("a == b: %d\n", quaternionEqual(a, b));
//    quaternionCopy(a, &b);
//    printf("a == b: %d\n", quaternionEqual(a, b));
//    quaternionDivision(b, &a);
//    printf("{%g, {%g, %g, %g}}\n", a.w, a.v[0], a.v[1], a.v[2]);
//    quaternionUnitize(&a);
//    printf("{%g, {%g, %g, %g}}\n", a.w, a.v[0], a.v[1], a.v[2]);
//    quaternionInverse(&a);
//    printf("{%g, {%g, %g, %g}}\n", a.w, a.v[0], a.v[1], a.v[2]);
//    quaternionFromRotate((Vector3){1, 0, 0}, M_PI / 4, &a);
    quaternionFromRotate((Vector3) {0, 1, 0}, M_PI / 4, &b, 0);
    quaternionMultiply(b, &a);
    printf("{%g, {%g, %g, %g}}\n", a.w, a.v[0], a.v[1], a.v[2]);
    printf("%g\n", vector4GetNorm(*(Vector4 *) &a));
    Vector3 v = {1, 0, 0};
    quaternionRotate(b, v);
    printf("{%g, %g, %g}\n", v[0], v[1], v[2]);

}