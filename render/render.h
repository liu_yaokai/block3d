//
// Created by Yaokai Liu on 2023/2/22.
//

#ifndef BLOCK3D_RENDER_H
#define BLOCK3D_RENDER_H

#include "debug.h"
#include <GLFW/glfw3.h>

GLFWwindow * game_window_init();
GLuint game_shader_init(const char * shader_dir);

int set_camera();
int compute_objects();

int game_render_update();

int game_terminate();
#endif //BLOCK3D_RENDER_H
