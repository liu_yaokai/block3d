//
// Created by Yaokai Liu on 2023/2/15.
//

#ifndef BLOCK3D_RUNTIME_H
#define BLOCK3D_RUNTIME_H

#include "IO.h"
#include "interacts.h"

int game_runtime_init();

Local * accessLocal(const int64_t target_position[3]);

int player_walk();
int player_shift_go();
int player_run();
int player_jump();
int player_update();

int area_update();
int game_process_events();

extern bool OUTSIDE_UPDATE;

#endif //BLOCK3D_RUNTIME_H
