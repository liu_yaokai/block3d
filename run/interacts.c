//
// Created by Yaokai Liu on 2023/2/21.
//

#include "interacts.h"

void set_speed_forward() {
    PLAYER_DIRECT_WEIGHT[0] += 1;
}

void set_speed_back() {
    PLAYER_DIRECT_WEIGHT[0] -= 1;
}

void set_speed_left() {
    PLAYER_DIRECT_WEIGHT[1] += 1;
}

void set_speed_right() {
    PLAYER_DIRECT_WEIGHT[1] -= 1;
}

void jump() {
    PLAYER_JUMPED = true;
}
