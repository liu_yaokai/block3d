//
// Created by Yaokai Liu on 2023/2/22.
//

#ifndef BLOCK3D_CONTEXT_H
#define BLOCK3D_CONTEXT_H

#include "Entity/Entity.h"
#include "Block/Block.h"
#include "Item/Item.h"
#include "local.h"

// System
extern const char * EXTENSION_DIR;
extern const char * WORLD_DIR;
extern const char * SHADER_DIR;
extern const char * TEXTURE_DIR;
extern const char * MODEL_DIR;

// World
extern char * WORLD_NAME;
extern int64_t DIM_ID;
extern int64_t FIELD_LOCATION[3];
extern int64_t SEMIA_LOCATION[3];
extern int64_t QUARTA_LOCATION[3];
extern char TERRAIN_QUARTA_PATH[512];
extern int8_t LOAD_RADIUS;
extern int8_t LOAD_HEIGHT;
extern int32_t LOCAL_COUNT;
extern Local * LOCAL_ARRAY;


// player
extern float PLAYER_POSITION[3];
extern float PLAYER_PLAIN_TOWARDS[2];
extern float PLAYER_UP_ANGLE;
extern float PLAYER_SPEED;
extern float PLAYER_MOVE_VECTOR[2];
extern float PLAYER_JUMP_HEIGHT;
extern float PLAYER_JUMP_SPEED;

extern int64_t PLAYER_LOCAL_ADDRESS[3];
extern uint8_t PLAYER_IN_LOCAL_ADDRESS[3];
extern uint8_t PLAYER_CHUNK_IN_LOCAL_ADDRESS[3];
extern uint8_t PLAYER_IN_CHUNK_ADDRESS[3];

extern uint8_t TARGET_IN_LOCAL_ADDRESS[3];
extern uint8_t TARGET_CHUNK_IN_LOCAL_ADDRESS[3];
extern uint8_t TARGET_IN_CHUNK_ADDRESS[3];


// Runtime

#endif //BLOCK3D_CONTEXT_H
