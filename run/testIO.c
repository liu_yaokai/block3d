//
// Created by Yaokai Liu on 2023/2/26.
//

#include "runtime.h"
#include <stdlib.h>

int main() {
    Local _local = {};
    _local.header.position[0] = 0;
    _local.header.position[1] = 0;
    _local.header.position[2] = 0;
    ChunkArraySBHeader * chunk_array = malloc(sizeof(ChunkArraySBHeader) + 256 * sizeof(ChunkStorageBlock));
    IndexTableSBHeader * index_table = malloc(sizeof(IndexTableSBHeader) + sizeof(uint64_t));
    LayerArraySBHeader * layer_array = malloc(sizeof(LayerArraySBHeader) + 256 * sizeof(LayerStorageBlock));
    CorpsArraySBHeader * corps_array = malloc(sizeof(CorpsArraySBHeader) + sizeof(CorpsSBHeader *));
    _local.header.chunk_array = chunk_array;
    _local.header.blocks = index_table;
    _local.header.layer_array = layer_array;
    _local.header.corps_array = corps_array;

    chunk_array->length = 256;
    for(int i = 0; i < 16; i ++) {
        for (int k = 0; k < 16; k ++) {
            _local.contents[i][0][k] = i + k * 16;

        }
    }


}