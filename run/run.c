//
// Created by Yaokai Liu on 2/12/ 17.
//

#include "render.h"
#include "runtime.h"
#include <stdio.h>

int game_launch() {

}

int game_context_init() {

}

int main(int argc, char *argv[]) {

    game_launch();
    GLFWwindow * window = game_window_init();
    load_extern_libraries(EXTENSION_DIR);

    game_runtime_init();
    load_world_summaries("demo");

    GLuint shader_program = game_shader_init(SHADER_DIR);

//    menu_loop();
//    if (new_world)
//        create_new_world();
//    else
//        load_world_data();

    load_terrain();
//    load_players_data();
    set_camera();
    compute_objects();
//    load_active_data();
    game_loop:
    {
        game_render_update();
        game_process_events();
        if (OUTSIDE_UPDATE) {
            set_camera();
        }
        area_update();
    }

//    menu_loop();

    game_terminate();
    return 0;
}
