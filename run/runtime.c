//
// Created by Yaokai Liu on 2023/2/15.
//

#include "runtime.h"
#include "math/Vector.h"

bool OUTSIDE_UPDATE = false;

int game_runtime_init() {

    // LOCAL_COUNT
    LOCAL_COUNT = (2 * LOAD_RADIUS + 1);
    LOCAL_COUNT *= LOCAL_COUNT;
    LOCAL_COUNT *= 2 * LOAD_HEIGHT + 1;

    return 0;
}

int player_walk() {
    PLAYER_DIRECT_WEIGHT[0] %= 3;
    PLAYER_DIRECT_WEIGHT[1] %= 3;
    PLAYER_DIRECT_WEIGHT[0] -= 1;
    PLAYER_DIRECT_WEIGHT[1] -= 1;
//    vector2Unitize(PLAYER_PLAIN_TOWARDS);

    PLAYER_MOVE_VECTOR[0] = (float)PLAYER_DIRECT_WEIGHT[0] * PLAYER_PLAIN_TOWARDS[0];
    PLAYER_MOVE_VECTOR[1] = (float)PLAYER_DIRECT_WEIGHT[1] * PLAYER_PLAIN_TOWARDS[1];
    vector2Scale(PLAYER_MOVE_VECTOR, PLAYER_SPEED);
    OUTSIDE_UPDATE = true;
    return 0;
}

int player_run() {
    PLAYER_DIRECT_WEIGHT[0] %= 3;
    PLAYER_DIRECT_WEIGHT[1] %= 3;
    PLAYER_DIRECT_WEIGHT[0] -= 1;
    PLAYER_DIRECT_WEIGHT[1] -= 1;
//    vector2Unitize(PLAYER_PLAIN_TOWARDS);

    PLAYER_MOVE_VECTOR[0] = (float)PLAYER_DIRECT_WEIGHT[0] * PLAYER_PLAIN_TOWARDS[0];
    PLAYER_MOVE_VECTOR[1] = (float)PLAYER_DIRECT_WEIGHT[1] * PLAYER_PLAIN_TOWARDS[1];
    vector2Scale(PLAYER_MOVE_VECTOR, 1.5f * PLAYER_SPEED);
    OUTSIDE_UPDATE = true;
    return 0;
}
int player_jump() {
    PLAYER_JUMP_SPEED =
    OUTSIDE_UPDATE = true;
    return 0;
}

int player_update() {
    return 0;
}

int area_update() {
    return 0;
}
int game_process_events() {
    return 0;
}

Local * accessLocal(const int64_t target_position[3]) {
    int64_t address[3];
    for (int i = 0; i < 3; i ++) {
        address[i] = target_position[i] >> 8;
    }
    for (int i = 0; i < LOCAL_COUNT; i ++) {
        if (LOCAL_ARRAY[i].header.position[0] == address[0]
         && LOCAL_ARRAY[i].header.position[1] == address[1]
         && LOCAL_ARRAY[i].header.position[2] == address[2])
            return &LOCAL_ARRAY[i];
    }
    return NULL;
}