//
// Created by Yaokai Liu on 2023/2/21.
//

#ifndef BLOCK3D_INTERACTS_H
#define BLOCK3D_INTERACTS_H

#include <stdbool.h>
#include <GLFW/glfw3.h>

typedef void action(void);

// Player status
static int8_t PLAYER_DIRECT_WEIGHT[2] = {};
static bool PLAYER_JUMPED = false;

void set_speed_forward();
void set_speed_back();
void set_speed_left();
void set_speed_right();
void jump();

static action * GAME_BASIC_KEY_ACTION_TABLE[] = {
        [GLFW_KEY_W] = set_speed_forward,
        [GLFW_KEY_S] = set_speed_back,
        [GLFW_KEY_A] = set_speed_left,
        [GLFW_KEY_D] = set_speed_right,
        [GLFW_KEY_SPACE] = jump,
};

extern action * const GAME_EXTERN_KEY_ACTION_TABLE[];

#endif //BLOCK3D_INTERACTS_H
