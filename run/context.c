//
// Created by Yaokai Liu on 2023/2/23.
//

#include "context.h"

// System
const char * EXTENSION_DIR = "../extends";
const char * WORLD_DIR = "../worlds";
const char * SHADER_DIR = "../shaders";
const char * TEXTURE_DIR = "../textures";
const char * MODEL_DIR = "../models";

// World
char * WORLD_NAME = NULL;
int64_t DIM_ID = 0;
int64_t FIELD_LOCATION[3] = {};
int64_t SEMIA_LOCATION[3] = {};
int64_t QUARTA_LOCATION[3] = {};
char TERRAIN_QUARTA_PATH[512] = {};
int8_t LOAD_RADIUS = 2;
int8_t LOAD_HEIGHT = 1;
int32_t LOCAL_COUNT = 0;
Local * LOCAL_ARRAY = NULL;

// Player
float PLAYER_POSITION[3] = {};
float PLAYER_PLAIN_TOWARDS[2] = {};
float PLAYER_UP_ANGLE = 0.0f;
float PLAYER_SPEED = 0.0f;
float PLAYER_MOVE_VECTOR[2] = {};
float PLAYER_JUMP_HEIGHT = 0.0f;
float PLAYER_JUMP_SPEED = 0.0f;
