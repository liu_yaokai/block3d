//
// Created by Yaokai Liu on 2023/2/15.
//

#include "IO.h"
#include <stdlib.h>
#include <stdio.h>

static int64_t CENTER_LOCAL_ADDRESS[3] = {};

int setTerrainPath() {
    char field_dir[64] = {}, semia_dir[64] = {}, quarta_dir[64] = {};
    sprintf(field_dir, "field_%llx_%llx_%llx",
            FIELD_LOCATION[0], FIELD_LOCATION[1], FIELD_LOCATION[2]);
    sprintf(semia_dir, "semia_%llx_%llx_%llx",
            SEMIA_LOCATION[0], SEMIA_LOCATION[1], SEMIA_LOCATION[2]);
    sprintf(quarta_dir, "quarta_%llx_%llx_%llx",
            QUARTA_LOCATION[0], QUARTA_LOCATION[1], QUARTA_LOCATION[2]);
    sprintf(TERRAIN_QUARTA_PATH, "%s/%s/DIM_%llx/%s/%s/%s",
            WORLD_DIR, WORLD_NAME, DIM_ID, field_dir, semia_dir, quarta_dir);
    return 0;
}

int setWorldPath(const char *world_name) {
    WORLD_NAME = (char *) world_name;
    char world_path[64] = {};
    sprintf(world_path, "%s/%s", WORLD_DIR, WORLD_NAME);
    return 0;
}

int load_world_summaries(const char *world_name) {

    setWorldPath(world_name);
//    FILE * world_file = fopen(world_path, "r");

    __load_address:
    FIELD_LOCATION[0] = 0;
    FIELD_LOCATION[1] = 0;
    FIELD_LOCATION[2] = 0;

    SEMIA_LOCATION[0] = 0;
    SEMIA_LOCATION[1] = 0;
    SEMIA_LOCATION[2] = 0;

    QUARTA_LOCATION[0] = 0;
    QUARTA_LOCATION[1] = 0;
    QUARTA_LOCATION[2] = 0;

    LOAD_RADIUS = 0;
    LOAD_HEIGHT = 0;

    for (int i = 0; i < 3; ++i) {
        CENTER_LOCAL_ADDRESS[i] = 0;
    }

    __load_player:
    PLAYER_POSITION[0] = 0.0f;
    PLAYER_POSITION[1] = 0.0f;
    PLAYER_POSITION[2] = 0.0f;

    PLAYER_UP_ANGLE = 0.0f;
    PLAYER_PLAIN_TOWARDS[0] = 0.0f;
    PLAYER_PLAIN_TOWARDS[1] = 0.0f;
    PLAYER_SPEED = 0.5f;

    __load_entities:
    return 0;
}

int load_terrain() {
    int16_t local_count = (int16_t) ((2 * LOAD_RADIUS + 1) * (2 * LOAD_RADIUS + 1));
    LOCAL_ARRAY = malloc((2 * LOAD_HEIGHT + 1) * local_count * sizeof(typeof(*LOCAL_ARRAY)));

    for (int i = -LOAD_RADIUS; i <= LOAD_RADIUS; i++) {
        for (int j = -LOAD_HEIGHT; j <= LOAD_HEIGHT; j++) {
            for (int k = -LOAD_RADIUS; k <= LOAD_RADIUS; k++) {
                int l_k = k + LOAD_RADIUS;
                int l_j = (j + LOAD_HEIGHT) * local_count;
                int l_i = (i + LOAD_RADIUS) * (2  * LOAD_RADIUS + 1);
                int index = l_i + l_j + l_k;
                LOCAL_ARRAY[index].position[0] = CENTER_LOCAL_ADDRESS[0] + i;
                LOCAL_ARRAY[index].position[1] = CENTER_LOCAL_ADDRESS[1] + j;
                LOCAL_ARRAY[index].position[2] = CENTER_LOCAL_ADDRESS[2] + k;
                Local * _local = &LOCAL_ARRAY[index];
                loadLocalContents(_local);
            }
        }
    }
    return 0;
}

int loadLocalContents(Local *_dest_local) {
    int64_t * address = _dest_local->position;
    char local_path[512] = {};
    setTerrainPath();
    sprintf(local_path, "%s/local_%llx_%llx_%llx",
            TERRAIN_QUARTA_PATH, address[0], address[1], address[2]);

    FILE * local_file = fopen(local_path, "rb");
    if (!local_file) {
        fprintf(stderr, "file '%s' not exists.", local_path);
        goto __failed_exception;
    }
    fseek(local_file, 0, SEEK_END);
    size_t length = ftell(local_file);
    if ( length < sizeof(LocalFileHeader)) {
        fprintf(stderr, "file '%s' is not a terrain file.", local_path);
        goto __failed_exception;
    }
    length = (length - 1) / sizeof(uint64_t) + 1;
    void * contents = malloc(length * sizeof(uint64_t));
    fread(contents, sizeof(uint64_t), length, local_file);
    int value = analyzeLocalFile(_dest_local, contents);
    free(contents);
    fclose(local_file);
    if (value != 0) {
        goto __failed_exception;
    }
    return 0;

    __failed_exception:
    return 1;
}

int saveLocalContents(Local * _local) {
    setTerrainPath();

    char local_path[512] = {};
    sprintf(local_path, "%s/local_%llx_%llx_%llx",
            TERRAIN_QUARTA_PATH,
            _local->position[0],
            _local->position[1],
            _local->position[2]);

    size_t file_length = 0;
    uint64_t * contents = composeLocalFile(_local, &file_length);
    if (!contents) {
        return 1;
    }
    FILE * local_file = fopen(local_path, "wb");
    size_t a = fwrite(contents, sizeof(uint64_t), file_length, local_file);
    if (a < file_length * sizeof(uint64_t)) {
        fclose(local_file);
        return 1;
    }
    return 0;
}

int load_extern_libraries(const char *extern_dir) {
    return 0;
}
