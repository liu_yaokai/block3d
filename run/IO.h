//
// Created by Yaokai Liu on 2023/2/15.
//

#ifndef BLOCK3D_IO_H
#define BLOCK3D_IO_H

#include "context.h"
#include "local.h"

#define QUEUE_BUFFER_SIZE (16 * 16)

int setTerrainPath();

int load_extern_object_data();

int load_extern_libraries(const char * extern_dir);

int load_world_summaries(const char * world_name);

int loadLocalContents(Local *_dest_local);
int saveLocalContents(Local * _local);

int load_terrain();
int save_terrain();

int load_world_data();
int save_world_data();

int load_players_data();
int save_players_data();

int load_active_data();
int save_active_data();

#endif //BLOCK3D_IO_H
