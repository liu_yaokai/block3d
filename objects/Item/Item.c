//
// Created by Yaokai Liu on 1/29/23.
//

#include "Item.h"
#include "ITEM_DATA.data"

typedef struct Item {
    item_t      type;
    ItemAttrStruct attribute;
} Item;

void item_init(Item * _item, item_t _type) {
    _item->type = _type;
    _item->attribute = ITEM_INIT_ATTRIBUTE_TABLE[_type];
}

item_t getItemType(Item * _item) {
    return _item->type;
}

void getItemTypeStaticAttr(Item * _item, ItemTypeStaticStruct * _ctr) {
    *_ctr = ITEM_TYPE_STATIC_TABLE[_item->type];
}

