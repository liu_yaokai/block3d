//
// Created by Yaokai Liu on 1/29/23.
//

#ifndef BLOCK3D_ITEM_H
#define BLOCK3D_ITEM_H

#include <stdint.h>
#include <stdbool.h>
#include "ITEM_LIST.data"

typedef enum item_list item_t;

typedef struct Item Item;

typedef void item_method(Item * the_item, int argc, void * argv[]);

typedef struct {
    uint32_t    model;
    uint8_t     stacked;
    char *      name;
    item_method ** methods;
} ItemTypeStaticStruct;

typedef struct {
    uint64_t id;
    uint8_t stacked;
    uint8_t _unused_[];
} ItemAttrStruct;

void item_init(Item * _item, item_t _type);

item_t getItemType(Item * _item);

void getItemTypeStaticAttr(Item * _item, ItemTypeStaticStruct * _ctr);

#endif //BLOCK3D_ITEM_H
