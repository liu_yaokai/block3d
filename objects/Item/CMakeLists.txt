cmake_minimum_required(VERSION 3.24)
project(Item C)

set(CMAKE_C_STANDARD 23)

add_library(Item
        ITEM_LIST.data
        ITEM_DATA.data
        Item.c Item.h)
