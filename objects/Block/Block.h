//
// Created by Yaokai Liu on 1/28/23.
//

#ifndef BLOCK3D_BLOCK_H
#define BLOCK3D_BLOCK_H

#include <stdint.h>
#include <stdbool.h>
#include "BLOCK_LIST.data"

typedef enum block_list block_t;

typedef struct Block Block;

typedef void block_method(Block * the_block, int argc, void * argv[]);

typedef struct  {
    char *  name;
    uint32_t modelID; // = block type.
    block_method ** methods;
    bool    powerful: 1;
    bool    breakable: 1;
    bool    movable: 1;
    bool    interactive: 1;
    bool    transparent: 1;
    bool    active: 1;
    uint8_t hardness;
} BlockTypeStaticStruct;

typedef struct {
    uint8_t powered;
    uint8_t towards;
    uint8_t _unused_[];
} BlockAttrStruct;

void block_init(Block *_block, block_t _type);
block_t getBlockType(Block * _block);
void getBlockTypeStaticAttr(Block * _block, BlockTypeStaticStruct * _ctr);
void setPowered(Block * _block, uint8_t power_level);
uint8_t getPowered(Block * _block);
void setTowards(Block * _block, uint8_t towards);
uint8_t getTowards(Block * _block);

#endif //BLOCK3D_BLOCK_H
