//
// Created by Yaokai Liu on 1/28/23.
//

#include "Block.h"
#include "BLOCK_DATA.data"

typedef struct Block {
    block_t         type;
    BlockAttrStruct attributes;
} Block;


void block_init(Block *_block, block_t _type) {
    _block->type = _type;
    _block->attributes = BLOCK_INIT_ATTRIBUTE_TABLE[_type];
}

block_t getBlockType(Block * _block) {
    return _block->type;
}

void getBlockTypeStaticAttr(Block * _block, BlockTypeStaticStruct * _ctr) {
    *_ctr = BLOCK_TYPE_STATIC_TABLE[_block->type];
}

void setPowered(Block * _block, uint8_t power_level) {
    _block->attributes.powered = power_level;
}

uint8_t getPowered(Block * _block) {
    return _block->attributes.powered;
}

void setTowards(Block * _block, uint8_t towards) {
    _block->attributes.towards = towards;
}
uint8_t getTowards(Block * _block) {
    return _block->attributes.towards;
}