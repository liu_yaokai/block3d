cmake_minimum_required(VERSION 3.24)
project(Block C)

set(CMAKE_C_STANDARD 23)

add_library(Block Block.c Block.h)