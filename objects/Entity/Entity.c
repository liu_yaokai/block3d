//
// Created by Yaokai Liu on 1/28/23.
//

#include "Entity.h"
#include "ENTITY_DATA.data"

void entity_init(Entity * _entity, entity_t _type, const double _position[3]) {
    _entity->type = _type;
    for (int i = 0; i < 3; i ++) {
        _entity->position[i] = _position[i];
    }
    _entity->attributes = ENTITY_INIT_ATTRIBUTE_TABLE[_type];
}

entity_t getEntityType(Entity * _entity) {
    return _entity->type;
}

void getEntityTypeStaticAttr(Entity * _entity, EntityTypeStaticStruct * _ctr) {
    *_ctr = ENTITY_TYPE_STATIC_TABLE[_entity->type];
}

void setId(Entity * _entity, const uint64_t id[4]) {
    for (int i = 0; i < 4; i++)
        _entity->attributes.id[i] = id[i];
}
