//
// Created by Yaokai Liu on 1/28/23.
//

#ifndef BLOCK3D_ENTITY_H
#define BLOCK3D_ENTITY_H

#include <stdint.h>
#include <stdbool.h>
#include "ENTITY_LIST.data"

typedef enum entity_list entity_t;

typedef struct Entity Entity;

typedef void entity_method(Entity *the_entity, int argc, void *argv[]);

typedef struct {
    uint32_t modelID;
    char *  name;
    entity_method ** methods;
} EntityTypeStaticStruct;

typedef struct {
    uint64_t id[4];
    uint8_t towards;
    uint8_t _unused_[];
} EntityAttrStruct;

void entity_init(Entity *_entity, entity_t _type,
                 const double _position[3]);

entity_t getEntityType(Entity *_entity);

void getEntityTypeStaticAttr(Entity *_entity,
                             EntityTypeStaticStruct *_ctr);

void setId(Entity * _entity, const uint64_t id[4]);

typedef struct Entity {
    entity_t  type;
    double    position[3];
    EntityAttrStruct  attributes;
} Entity;

#endif //BLOCK3D_ENTITY_H
