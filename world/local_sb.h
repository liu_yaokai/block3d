//
// Created by Yaokai Liu on 2023/2/27.
//

#ifndef BLOCK3D_LOCAL_SB_H
#define BLOCK3D_LOCAL_SB_H
#include <stdint.h>
#include "Block/Block.h"

#ifdef INDEX_USING_TWO_BYTE
typedef uint16_t index_t;
#else
typedef uint8_t index_t;
#endif

struct IndexTable {
    uint64_t    length;
    block_t     table[];
};

typedef struct IndexTable IndexTableSBHeader __attribute__((aligned(sizeof(uint64_t))));

enum layer_type {
    pure_follow_x,
    pure_follow_y,
    pure_follow_z,
    pure_follow_xy,
    pure_follow_yz,
    pure_follow_zx,
    pure_follow_yx,
    pure_follow_xz,
    pure_follow_zy,
};

typedef struct {
    enum layer_type type: 4;
    uint8_t order: 4;
    index_t block;
} LayerStorageBlock;

struct LayerArray {
    uint64_t length;
    LayerStorageBlock layers[];
};

typedef struct LayerArray LayerArraySBHeader __attribute__((aligned(sizeof(uint64_t))));

struct Corps {
    uint8_t  dx: 4;
    uint8_t  dy: 4;
    uint8_t  dz: 4;
    uint8_t  a: 4;
    uint8_t  b: 4;
    uint8_t  c: 4;
    index_t  blocks[];
};

typedef struct Corps CorpsSBHeader __attribute__((aligned(sizeof(uint64_t))));

typedef struct {
    uint64_t length;
    ptrdiff_t corps[];
} CorpsArraySBHeader __attribute__((aligned(sizeof(uint64_t))));

enum chunk_type {
    pure_chunk = 0,
    layer_chunk = 1,
    corps_chunk = 2,
    mix_chunk = 3
};

typedef struct {
    enum chunk_type type: 4;
    uint8_t dx: 4;
    uint8_t dy: 4;
    uint8_t dz: 4;
    index_t basic_index;
    uint8_t layer_count;
    uint8_t corps_count;
    index_t index_count;
    ptrdiff_t index_offset;
    ptrdiff_t layer_offset;
    ptrdiff_t corps_offset;
} ChunkStorageBlock __attribute__((aligned(sizeof(uint64_t))));

typedef struct {
    uint64_t length;
    ChunkStorageBlock chunks[];
} ChunkArraySBHeader __attribute__((aligned(sizeof(uint64_t))));

typedef struct {
    uint32_t content_length;
    ptrdiff_t chunk_array_offset;
    ptrdiff_t index_table_offset;
    ptrdiff_t layer_array_offset;
    ptrdiff_t corps_array_offset;
} LocalFileHeader __attribute__((aligned(sizeof(uint64_t))));

#endif //BLOCK3D_LOCAL_SB_H
