//
// Created by Yaokai Liu on 1/29/23.
//

#ifndef BLOCK3D_LOCAL_H
#define BLOCK3D_LOCAL_H

#include "local_sb.h"
#include "local_me.h"

int analyzeLocalFile(Local *_local, uint64_t * _contents);
uint64_t * composeLocalFile(Local *_local, size_t * _length);


#endif //BLOCK3D_LOCAL_H
