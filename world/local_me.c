//
// Created by Yaokai Liu on 2023/2/27.
//

#include "local_me.h"
#include <stdlib.h>
#include <memory.h>

int addBlockNode(BlockTreeNode * _node, block_t _block, index_t _index) {
    if (_block == _node->block) {
        return (_node->index == _index) ? _index : -1;
    }
    if (_block > _node->block) {
        if (!_node->right) {
            _node->right = calloc(1, sizeof(BlockTreeNode));
            _node->right->block = _block;
            _node->right->index = _index;
            return _node->right->index;
        }
        return addBlockNode(_node->right, _block, _index);
    }
    if (_block < _node->block) {
        if (!_node->left) {
            _node->left = calloc(1, sizeof(BlockTreeNode));
            _node->left->block = _block;
            _node->left->index = _index;
            return _node->left->index;
        }
        return addBlockNode(_node->left, _block, _index);
    }
    return -1;
}
int addBlockType(Chunk *_chunk, block_t _block, index_t _index) {
    if (!_chunk->blocks->root) {
        _chunk->blocks->root = calloc(1, sizeof(BlockTreeNode));
        _chunk->blocks->root->block = _block;
        _chunk->blocks->root->index = _index;
        return _chunk->blocks->root->index;
    }
    return addBlockNode(_chunk->blocks->root, _block, _index);
}


int addLayerNode(LayerTreeNode *_node, Layer * _layer) {
    if (_layer->order == _node->layer.order) {
        return (_node->layer.block == _layer->block) ? _layer->order : -1;
    }
    if (_layer->order > _node->layer.order) {
        if (!_node->right) {
            _node->right = calloc(1, sizeof(LayerTreeNode));
            memcpy(&(_node->right->layer), _layer, sizeof(Layer));
            return _node->right->layer.order;
        }
        return addLayerNode(_node->right, _layer);
    }
    if (_layer->order < _node->layer.order) {
        if (!_node->left) {
            _node->left = calloc(1, sizeof(LayerTreeNode));
            memcpy(&(_node->left->layer), _layer, sizeof(Layer));
            return _node->left->layer.order;
        }
        return addLayerNode(_node->left, _layer);
    }
    return -1;
}
int addLayer(Chunk *_chunk, Layer * _layer) {
    if (!_chunk->layers->root) {
        _chunk->layers->root = calloc(1, sizeof(LayerTreeNode));
        memcpy(&(_chunk->layers->root->layer), _layer, sizeof(Layer));
        return _layer->order;
    }
    return addLayerNode(_chunk->layers->root, _layer);
}


int addCorpsNode(CorpsTreeNode *_node, Corps * _corps) {
    int c_order = _corps->dx + _corps->dz * 16 + _corps->dy * 16 * 16;
    int n_order = _node->corps->dx + _node->corps->dz * 16 + _node->corps->dy * 16 * 16;
    if (c_order == n_order) {
        return (_node->corps == _corps) ? c_order : -1;
    }
    if (c_order > n_order) {
        if (!_node->right) {
            _node->right = calloc(1, sizeof(CorpsTreeNode));
            _node->right->corps = _corps;
            return c_order;
        }
        return addCorpsNode(_node->right, _corps);
    }
    if (c_order < n_order) {
        if (!_node->left) {
            _node->left = calloc(1, sizeof(CorpsTreeNode));
            _node->left->corps = _corps;
            return c_order;
        }
        return addCorpsNode(_node->left, _corps);
    }
    return -1;
}
int addCorps(Chunk *_chunk, Corps * _corps) {
    if (!_chunk->corps->root) {
        _chunk->corps->root = calloc(1, sizeof(CorpsTreeNode));
        _chunk->corps->root->corps = _corps;
        return _corps->dx + _corps->dz * 16 + _corps->dy * 16 * 16;
    }
    return addCorpsNode(_chunk->corps->root, _corps);
}


void releaseBlockNode(BlockTreeNode * _node) {
    if (_node->left) {
        releaseBlockNode(_node->left);
    }
    if (_node->right) {
        releaseBlockNode(_node->right);
    }
    free(_node);
}
void releaseBlockTree(BlockTree * _tree) {
    releaseBlockNode(_tree->root);
}

void releaseLayerNode(LayerTreeNode * _node) {
    if (_node->left) {
        releaseLayerNode(_node->left);
    }
    if (_node->right) {
        releaseLayerNode(_node->right);
    }
    free(_node);
}
void releaseLayerTree(LayerTree * _tree) {
    releaseLayerNode(_tree->root);
}

void releaseCorpsNode(CorpsTreeNode * _node) {
    if (_node->left) {
        releaseCorpsNode(_node->left);
    }
    if (_node->right) {
        releaseCorpsNode(_node->right);
    }
    free(_node);
}
void releaseCorpsTree(CorpsTree * _tree) {
    releaseCorpsNode(_tree->root);
}
