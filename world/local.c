//
// Created by Yaokai Liu on 1/29/23.
//

#include "local.h"
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <minmax.h>

Chunk * chunkSB2ME(ChunkStorageBlock * _chunk_sb, IndexTableSBHeader * _index_table, LayerArraySBHeader * _layer_array, CorpsArraySBHeader * _corps_array) {
    Chunk * chunk = calloc(1, sizeof(Chunk));
    chunk->type = _chunk_sb->type;
    chunk->dx = _chunk_sb->dx;
    chunk->dy = _chunk_sb->dy;
    chunk->dz = _chunk_sb->dz;
    chunk->basic_index = _chunk_sb->basic_index;

    if (_chunk_sb->index_offset + _chunk_sb->index_count > _index_table->length) {
        goto __failed_convert;
    }
    chunk->blocks = calloc(1, sizeof(BlockTree));
    for (index_t i = 0; i < _chunk_sb->index_count; i ++) {
        addBlockType(chunk, _index_table->table[i], i);
    }
    chunk->index_table = malloc(sizeof(IndexTable) + _chunk_sb->index_count * sizeof(index_t));
    chunk->index_table->length = _chunk_sb->index_count;
    memcpy(chunk->index_table->table,
           _index_table->table + _chunk_sb->index_offset,
           _chunk_sb->index_count * sizeof(index_t));

    if (_chunk_sb->layer_offset + _chunk_sb->layer_count > _layer_array->length) {
        goto __failed_convert;
    }
    chunk->layers = calloc(1, sizeof(LayerTree));
    for (int i = 0; i < _chunk_sb->layer_count; i ++) {
        addLayer(chunk, _layer_array->layers + _chunk_sb->layer_offset + i);
    }


    if (_chunk_sb->corps_offset + _chunk_sb->corps_count > _corps_array->length) {
        goto __failed_convert;
    }
    chunk->corps = calloc(1, sizeof(CorpsTree));
    for (int i = 0; i < _chunk_sb->corps_count; i ++) {
        addCorps(chunk, _corps_array->corps[_chunk_sb->corps_count + i] + (Corps *) NULL);
    }

    return chunk;

    __failed_convert:
    if (chunk->index_table) {
        free(chunk->index_table);
    }
    if (chunk->blocks) {
        releaseBlockTree(chunk->blocks);
    }
    if (chunk->layers) {
        releaseLayerTree(chunk->layers);
    }
    if (chunk->corps) {
        releaseCorpsTree(chunk->corps);
    }
    free(chunk);
    return NULL;
}

int analyzeLocalFile(Local *_local, uint64_t *_contents) {
    LocalFileHeader * local_header = (LocalFileHeader *) _contents;
    ChunkArraySBHeader * chunk_array = (ChunkArraySBHeader *) _contents + local_header->chunk_array_offset;
    IndexTableSBHeader * index_table = (IndexTableSBHeader *) _contents + local_header->index_table_offset;
    LayerArraySBHeader * layer_array = (LayerArraySBHeader *) _contents + local_header->layer_array_offset;
    CorpsArraySBHeader * corps_array = (CorpsArraySBHeader *) _contents + local_header->corps_array_offset;

    // Real Corps
    for (uint64_t i = 0; i < corps_array->length; i++) {
        CorpsSBHeader * corps_sb = (CorpsSBHeader *) _contents + corps_array->corps[i];
        size_t size = corps_sb->a * corps_sb->b * corps_sb->c;
        size = sizeof(Corps) + sizeof(index_t) * size;
        Corps * corps = malloc(size);
        memcpy(corps, corps_sb, size);
        corps_array->corps[i] = corps - (Corps *) NULL;
    }

    // Chunk Array
    for (int i = 0; i < min(chunk_array->length, 16 * 16 * 16); i ++) {
        ChunkStorageBlock * chunk_sb = &(chunk_array->chunks[i]);
        Chunk * chunk = chunkSB2ME(chunk_sb, index_table, layer_array, corps_array);
        if (!chunk) {
            goto __failed_analyse;
        }
        _local->contents[chunk->dx][chunk->dy][chunk->dz] = chunk;
    }

    // TODO: active blocks

    return 0;
    __failed_analyse:
    for (uint64_t i = 0; i < corps_array->length; i++) {
        Corps * corps = corps_array->corps[i] + (Corps *) NULL;
        free(corps);
    }
    return -1;
}

uint64_t * composeLocalFile(Local *_local, size_t * _length) {
    // TODO
    return NULL;
}

