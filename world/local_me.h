//
// Created by Yaokai Liu on 2023/2/27.
//

#ifndef BLOCK3D_LOCAL_ME_H
#define BLOCK3D_LOCAL_ME_H

#include "local_sb.h"

typedef struct IndexTable IndexTable;

typedef struct BlockTreeNode BlockTreeNode;
typedef struct BlockTreeNode {
    block_t block;
    index_t index;
    BlockTreeNode * left;
    BlockTreeNode * right;
} BlockTreeNode;
typedef struct {
    uint64_t depth;
    BlockTreeNode * root;
} BlockTree;

typedef LayerStorageBlock Layer;
typedef struct LayerTreeNode LayerTreeNode;
typedef struct LayerTreeNode {
    Layer layer;
    LayerTreeNode * left;
    LayerTreeNode * right;
} LayerTreeNode;
typedef struct {
    uint64_t depth;
    LayerTreeNode * root;
} LayerTree;

typedef struct Corps Corps;
typedef struct CorpsTreeNode CorpsTreeNode;
typedef struct CorpsTreeNode {
    Corps * corps;
    CorpsTreeNode * left;
    CorpsTreeNode * right;
} CorpsTreeNode;
typedef struct {
    uint64_t depth;
    CorpsTreeNode * root;
} CorpsTree;

typedef struct {
    enum chunk_type type: 4;
    uint8_t dx: 4;
    uint8_t dy: 4;
    uint8_t dz: 4;
    index_t basic_index;
    LayerTree * layers;
    CorpsTree * corps;
    BlockTree * blocks;
    IndexTable * index_table;
} Chunk;

typedef struct {
    int64_t position[3];
    Chunk * contents[16][16][16];
} Local;

void chunkUpdate(LocalFileHeader * _local, uint16_t chunk_addr);
Chunk * accessChunk(Local * _local, const int64_t target_position[3]);
block_t accessBlock(Local * _local, int64_t target_position[3]);

int addBlockType(Chunk *_chunk, block_t _block, index_t _index);
int addLayer(Chunk *_chunk, Layer * _layer);
int addCorps(Chunk *_chunk, Corps * _corps);

void releaseBlockTree(BlockTree * _tree);
void releaseLayerTree(LayerTree * _tree);
void releaseCorpsTree(CorpsTree * _tree);

#endif //BLOCK3D_LOCAL_ME_H
