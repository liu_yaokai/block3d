cmake_minimum_required(VERSION 3.24)
project(world C)

set(CMAKE_C_STANDARD 23)

add_library(local
        local_me.c local_me.h
        local_sb.c local_sb.h
        local.c local.h)
target_link_libraries(local Block)
